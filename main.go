package main

import (
	"fmt"
	"time"
	"net/http"
	"github.com/labstack/echo/v4"
	"os"
	"io"
	"gitlab.com/Ashik-2001/go-app-tutorial/internal/myapp/handler"
)



func main() {
	e := echo.New()
	handler := handler.New()
	e.POST("/users", handler.createUser)

	e.GET("/users/:id", handler.getUser)
	e.GET("/show", show)
	e.GET("/show_cookie", readCookie)
	e.POST("/save", save)
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.Logger.Fatal(e.Start(":8000"))
	//e.POST("/users", saveUser)
}

/*func createUser(c echo.Context) error {
	type User struct {
	Name  string `json:"name" xml:"name" form:"name" query:"name"`
	Email string `json:"email" xml:"email" form:"email" query:"email" validate:"required"`
	//Id string `json:"id" xml:"id" form:"id"`
}
	u := new(User)

	if err := c.Bind(u); err != nil {
		return err
	}
	return c.JSONPretty(http.StatusOK, u, " ")
	// or
	// return c.XML(http.StatusCreated, u)
	
}*/
func writeCookie(c echo.Context) error {
	cookie := new(http.Cookie)
	cookie.Name = "username"
	cookie.Value = "jon"
	cookie.Expires = time.Now().Add(24 * time.Hour)
	c.SetCookie(cookie)
	return c.String(http.StatusOK, "write a cookie")
}
func readCookie(c echo.Context) error {
	cookie, err := c.Cookie("username")
	if err != nil {
		return err
	}
	fmt.Println(cookie.Name)
	fmt.Println(cookie.Value)
	return c.String(http.StatusOK, "read a cookie")
}


/*func getUser(c echo.Context) error {
  	// User ID from path `users/:id`
  	id := c.Param("id")
  	//return c.Redirect(http.StatusMovedPermanently, "https://google.co.in")
	return c.HTML(http.StatusOK, "<strong>"+id+"</strong>")
}*/

func show(c echo.Context) error {
	team := c.QueryParam("team")
	member := c.QueryParam("member")
	return c.String(http.StatusOK, "team:" + team + ",member" + member)
}

func save(c echo.Context) error {
	name := c.FormValue("name")
	avatar, err := c.FormFile("avatar")
	if err != nil {
		return err
	}
	src, err := avatar.Open()
	if err != nil {
		return err
	}
	defer src.Close()
	dst, err := os.Create(avatar.Filename)
	if err != nil {
		return err
	}
	defer dst.Close()
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}
	return c.HTML(http.StatusOK, "<b> Thank YOu "+name+" </b>")
}